## Komputer Store
Welcome to the Komputer Store! This is a simple web application that allows you to browse and purchase laptops.

Features
Browse a list of laptops with their specifications and images
View the details of a selected laptop
Buy a laptop with a given bank balance
Apply for a loan to buy a laptop
Repay an outstanding loan
Work to get money

## Requirements
A modern web browser that supports HTML, CSS, and JavaScript
Internet connection to access the API and images

## Installation
Download the source code from the repository
Open the index.html file in a web browser
Start browsing and purchasing laptops!

## API
The data for the laptops is provided through a RESTful API. The API endpoint is https://hickory-quilled-actress.glitch.me/computers. The API returns an array of laptops, each with the following properties:

## Contributing
Fotis Staikos https://gitlab.com/f.staikos7
