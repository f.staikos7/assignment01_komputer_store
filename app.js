//initialize variables
let bankBalance = 500;
let outstandingLoan = 0;
let hasLoan = false;
let pay = 0;

//function that checks loan status, amount and if you can get it
function getLoan() {
  if (hasLoan) {
    alert(
      "You cannot have two loans at once. Please repay the outstanding loan first."
    );
  } else {
    let loanAmount = prompt("Enter the loan amount:");
    loanAmount = parseFloat(loanAmount);
    if (isNaN(loanAmount)) {
      alert("Invalid loan amount. Please enter a valid number.");
    } else if (loanAmount > bankBalance * 2) {
      alert("You cannot get a loan more than double of your bank balance.");
    } else {
      outstandingLoan = loanAmount;
      hasLoan = true;
      showRepayLoanButton();
      alert("Loan of " + loanAmount + "$" + " successfully acquired.");
      document.getElementById("outstandingLoan").textContent =
        outstandingLoan + "$";
    }
  }
}

//function that adds 100$ to your earnings
function work() {
  pay += 100;
  alert("You have earned " + pay + " from work.");
  document.getElementById("pay").innerHTML = pay + "$";
}

//function that makes a bank that you can transfer your money and pay your loan
function bank() {
  if (hasLoan) {
    let loanPayment = pay * 0.1;
    if (loanPayment > outstandingLoan) {
      pay -= outstandingLoan;
      outstandingLoan = 0;
      hasLoan = false;
      showRepayLoanButton();
    } else {
      pay -= loanPayment;
      outstandingLoan -= loanPayment;
    }
  }
  bankBalance += pay;

  pay = 0;
  alert(
    "Your salary has been successfully transferred to your bank balance. Your current bank balance is " +
      bankBalance +
      "$" +
      "."
  );
  document.getElementById("bankBalance").innerHTML = bankBalance + "$";
}

//function that lets you pay your loan
function repayLoan() {
  if (!hasLoan) {
    alert("You do not have any outstanding loans.");
  } else {
    if (pay >= outstandingLoan) {
      pay -= outstandingLoan;
      outstandingLoan = 0;
      document.getElementById("outstandingLoan").textContent =
        outstandingLoan + "$";
      hasLoan = false;
      showRepayLoanButton();
      alert(
        "Your loan has been successfully repaid. You have " +
          pay +
          "$" +
          " left after repayment."
      );
      document.getElementById("pay").innerHTML = pay + "$";
    } else {
      outstandingLoan -= pay;
      pay = 0;
      alert(
        "Your salary is not enough to fully repay the loan. " +
          outstandingLoan +
          "$" +
          " remains to be paid."
      );
      document.getElementById("outstandingLoan").textContent =
        outstandingLoan + "$";
    }
  }
}

//function that gets the Laptops from the json file
// Get the laptop data from the API endpoint
fetch("https://hickory-quilled-actress.glitch.me/computers")
  .then((response) => response.json())
  .then((laptops) => {
    // Populate the select box with the laptops
    let select = document.getElementById("laptops-select");
    laptops.forEach((laptop) => {
      let option = document.createElement("option");
      option.value = laptop.id;
      option.text = laptop.title;
      select.add(option);
    });

    // Display the information for the selected laptop
    select.addEventListener("change", (event) => {
      let selectedLaptop = laptops.find(
        (laptop) => laptop.id == event.target.value
      );
      document.getElementById(
        "laptop-image"
      ).src = `https://hickory-quilled-actress.glitch.me/${selectedLaptop.image}`;
      document.getElementById("laptop-name").textContent = selectedLaptop.title;
      document.getElementById("laptop-description").textContent =
        selectedLaptop.description;
      document.getElementById(
        "laptop-price"
      ).textContent = `$${selectedLaptop.price}`;

      // Clear the previous laptop specs
      let specsList = document.getElementById("laptop-specs");
      while (specsList.firstChild) {
        specsList.removeChild(specsList.firstChild);
      }

      // Add the new laptop specs
      selectedLaptop.specs.forEach((spec) => {
        let listItem = document.createElement("li");
        listItem.textContent = spec;
        specsList.appendChild(listItem);
      });
    });

    // Handle the buy now button click
    document.getElementById("buy-now").addEventListener("click", (event) => {
      let selectedLaptop = laptops.find((laptop) => laptop.id == select.value);
      if (bankBalance >= selectedLaptop.price) {
        bankBalance -= selectedLaptop.price;
        document.getElementById("bankBalance").innerHTML = bankBalance + "$";
        alert(
          `You have successfully bought a ${selectedLaptop.title}! Your new bank balance is $${bankBalance}.`
        );
      } else {
        alert(
          `You cannot afford the ${selectedLaptop.title}. Your current bank balance is $${bankBalance}.`
        );
      }
    });
  });
